package backend;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler; 
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import java.util.*;

public class Login extends Application {
	
	public void init() {
		
		initialise_controls();

		// set horizontal and vertical gaps for the gridpane
		gp.setHgap(5);
		gp.setVgap(5);
		
		// Add controls to gridpane
		gp.add(username_lbl, 1, 1);
		gp.add(username_txt, 2, 1);
		gp.add(password_lbl, 1, 2);
		gp.add(password_txt, 2, 2);
		gp.add(btn_login, 1, 3);
		gp.add(btn_reset, 2, 3);
		
		
		// Listener for btn_login to perform login
		btn_login.setOnAction(new EventHandler<ActionEvent>() {
			// override default method
			@Override
			public void handle(ActionEvent event) {
				login();
			}
		});
		
	}
	
	/**
	 * Login button action
	 */
	private void login(){
		System.out.println("login button clicked");
		
		// get username and password from textfields
		String userName = username_txt.getText(); 
		String password = password_txt.getText();
		
		// instantiate to users class
		Users user = new Users();
		Boolean resp = user.login(userName, password);
		if (resp) {
			System.out.println("Login was successful");
		}else{
			System.out.println("Invalid username or password.");
		}
		
	}
	
	private void initialise_controls() {
		// initialize Labels
		username_lbl = new Label("Username");
		password_lbl = new Label("Password");

		// initialize textfields
		username_txt = new TextField();
		password_txt = new PasswordField();

		// initialize buttons
		btn_login = new Button("Login");
		btn_reset = new Button("Reset");

		// Initialize gridpane
		gp = new GridPane();
	}
	
	// Overidden start method
	@Override
	public void start(Stage primaryStage) {
		// set a title on the window, set a scene, size, and show the window
		Scene scene = new Scene(gp,400,300);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		
		primaryStage.setTitle("Smart POS");
		// set window size
		primaryStage.setScene(scene);

		primaryStage.show();
	}
	
	// Overridden stop method - add a status message to this
	@Override
	public void stop() {
		System.out.println("Application is stopping");
	}
	
	// Entry point to our program
	public static void main(String[] args) {
		launch(args);
	}
	
	private GridPane gp;
	
	// username and password label
	private Label username_lbl;
	private Label password_lbl;
	
	// username and password textfield
	private TextField username_txt;
	private PasswordField password_txt;
	
	// buttons
	private Button btn_login;
	private Button btn_reset;

}
