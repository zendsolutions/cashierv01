package backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Users implements DAO {
	
	public Users(){
		
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getAccount_type() {
		return account_type;
	}
	public void setAccount_type(int account_type) {
		this.account_type = account_type;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	public String getPassword() {
		return password;
	}
	
	public int getStatus() {
		return status;
	}
	
	public boolean login(){
		return false;
	}

	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	public boolean createAccount(){
		
		return false;
	}
	
	/**
	 * verify and authenticate username and password supplied
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean login(String username, String password) {
		Boolean response = false;
		
		String sql = "Select * from users where username=? and password=?";
		
		try {
			Class.forName(DRIVER);
			Connection con = DriverManager.getConnection (DB_URL, USER, PASS);
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, username);
			stmt.setString(2, password);
			ResultSet rs = stmt.executeQuery();
			
			System.out.println(rs.toString());
			
			while (rs.next()){
				System.out.println(rs.toString());
				response = true;
			}
			
		} catch (ClassNotFoundException | SQLException ex) {
			// TODO: handle exception
			System.out.println(ex.getMessage());
		}
		return response;
	}
	
	private String username;
	private String first_name;
	private String last_name;
	private String email;
	private String password;
	private int account_type;
	private int status;
	private long phone;
		
}
